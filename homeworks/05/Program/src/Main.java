import java.util.Scanner;

class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int digit = 0;
        int minDigit = 9;
        if (a == -1) {
            System.out.println("Нет решения");
            minDigit = -1;
        }
        if (a == 0) {
            minDigit = 0;
        }
        while (a != -1) {
            a = Math.abs(a);
            while (a != 0) {
                digit = a % 10;
                if (digit < minDigit) {
                    minDigit = digit;
                }
                a = a / 10;
            }
            a = scanner.nextInt();
            if (a == 0) {
                minDigit = 0;
            }
        }
        System.out.println("Result: " + minDigit);
    }
}